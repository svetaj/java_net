import java.net.*;
import java.io.*;

public class FilterThread extends Thread {

    DataOutputStream out = null;
    DataInputStream in = null;

    public FilterThread(String str, DataOutputStream o, DataInputStream i) {
        super(str);
        out = o;
        in = i; 
        System.out.println("Created new thread "+getName());
    }

    public void run() {
        try {
            byte[] data = new byte[512];
            int no_data;
            System.out.println(getName()+": data I/O start");
            while ((no_data = in.read(data)) != -1) {
                System.out.write(data, 0, no_data);
                out.write(data, 0, no_data);
                out.flush();
            }
            System.out.println(getName()+": data I/O end");
        } 
        catch (IOException e) {
            System.err.println(getName()+": Socket I/O error");
        }
    }
}
