import java.net.*;
import java.io.*;

public class FilterThread extends Thread {

    DataOutputStream out = null;
    DataInputStream in = null;
    boolean direction = false;
    // 123456FA3853CFDCAC5DCBE0C044DC1429CDCDD1

    public FilterThread(String str, DataOutputStream o, DataInputStream i) {
        super(str);
        out = o;
        in = i; 
        System.out.println("Created new thread "+getName());
        if (getName().equals("socket1_to_socket2")) direction = true;
    }

    public void run() {
        try {
            byte[] data = new byte[512];
            int no_data;
            System.out.println("\n"+getName()+": data I/O :");
            while ((no_data = in.read(data)) != -1) {
                    System.out.write((new String(data, 0, no_data)).getBytes(), 0, no_data);
                    out.write((new String(data, 0, no_data)).getBytes(), 0, no_data);
                    out.flush();
               }
            System.out.println(getName()+": data I/O end");
        } 
        catch (IOException e) {
            System.err.println(getName()+": Socket I/O error");
        }
    }
}
