import java.net.*;
import java.io.*;
import java.lang.*;

public class Filter {

        final static byte ENQ = 0x05;
        final static byte CR = 0x0D;
        final static byte LF = 0x0A;
        final static byte XO = 0x4F;
        final static byte XK = 0x4B;
        final static byte XC = 0x43;
        final static byte XN = 0x4E;
        final static byte XE = 0x45;
        final static byte XT = 0x54;
        final static byte PL = 0x2B;
        final static byte XP = 0x50;
        final static byte XI = 0x49;
        final static byte CL = 0x3A;
        final static byte BL = 0x20;
        final static byte XR = 0x52;
        final static byte XD = 0x44;
        final static byte XY = 0x59;
        final static byte XG = 0x47;
        final static byte X0 = 0x30;
        final static byte CO = 0x2C;
        final static byte X1 = 0x31;
        final static byte XA = 0x41;
        final static byte XZ = 0x5A;
        final static byte XQ = 0x51;
        final static byte XV = 0x56;
        final static byte XB = 0x42;
        final static byte XS = 0x53;
        final static byte EQ = 0x3D;
        final static byte X7 = 0x37;
        final static byte X9 = 0x39;
        final static byte X6 = 0x36;
        final static byte SL = 0x2F;
        final static byte XL = 0x4C;

    public static void main(String[] args) throws IOException {

        DataOutputStream out1 = null;
        DataOutputStream out2 = null;
        DataInputStream in1 = null;
        DataInputStream in2 = null;
        ServerSocket serverSocket1 = null;
        Socket clientSocket1 = null;
        Socket clientSocket2 = null;

        if (args.length == 0) {
            System.out.println("java Filter IPaddr1 port1 IPaddr2 port2");
            System.exit(1);
        }

        String ipaddr1 = args[0];
        int port1 = new Integer(args[1]).intValue();
        String ipaddr2 = args[2];
        int port2 = new Integer(args[3]).intValue();

        while (true) {

        try {
            System.out.println("START:   creating server socket "+ipaddr2+", port "+port2);
            serverSocket1 = new ServerSocket(port2);
            System.out.println("SUCCESS: creating server socket "+ipaddr2+", port "+port2);
        } catch (IOException e) {
            System.err.println("Could not listen on port: "+port2+".");
            System.exit(1);
        }

        try {
            System.out.println("START:   accepting connection to socket "+ipaddr2+", port "+port2);
            clientSocket1 = serverSocket1.accept();
            System.out.println("SUCCESS: accepting connection to socket "+ipaddr2+", port "+port2);
            out1 = new DataOutputStream(clientSocket1.getOutputStream());
            in1 = new DataInputStream(clientSocket1.getInputStream());
        } catch (IOException e) {
            System.err.println("Accept failed.");
            System.exit(1);
        }

        boolean mcc = modemConnect(in1, out1);
        System.out.println("*************** modem connect = " + mcc);

        try {
            System.out.println("START:   connecting to socket "+ipaddr1+", port "+port1);
            clientSocket2 = new Socket(ipaddr1, port1);
            System.out.println("SUCCESS: connecting to socket "+ipaddr1+", port "+port1);
            out2 = new DataOutputStream(clientSocket2.getOutputStream());
            in2 = new DataInputStream(clientSocket2.getInputStream());
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: "+ipaddr1+".");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to: "+ipaddr1+".");
            System.exit(1);
        }

        FilterThread ft1 = new FilterThread(">>>>>>", out2, in1);
        FilterThread ft2 = new FilterThread("<<<<<<", out1, in2);
        ft1.start();
        if (ft1 == null) System.out.println("Filter: >>>>>>: null");
        ft2.start();
        if (ft2 == null) System.out.println("Filter: <<<<<<: null");

        while (ft1.isAlive() && ft2.isAlive())
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {}

        System.out.println("@@@@@@@@@ Closing I/O streams and sockets");
        out1.close();
        in1.close();
        out2.close();
        in2.close();
        clientSocket1.close();
        serverSocket1.close();
        clientSocket2.close();
	    }
    }

    static boolean modemConnect(DataInputStream i1, DataOutputStream o1) {
       boolean mc = false;
       try {
            byte[] data = new byte[512];
            byte[] data1 = new byte[512];
            int no_data;
            String message = null;
            String reply = null;

             while ((no_data = i1.read(data)) != -1) {
                message = new String(data, 0, no_data, "US-ASCII");
                reply = new String(data, 0, no_data, "US-ASCII");
                System.out.println("=======================>");
                System.out.write(data, 0, no_data);
                System.out.println("<=======================");
                if (message.indexOf("ATZQ0V1E0") >= 0) {
                   System.out.println("\nSent OK");
                   data1[0] = XO;
                   data1[1] = XK;
                   data1[2] = CR;
                   data1[3] = LF;
                   o1.write(data1, 0, 4);
                   o1.flush();
                   System.out.println("@@@@@@@@@@@@@@@@@@@@@@@>");
                   System.out.write(data1, 0, 4);
                   System.out.println("<@@@@@@@@@@@@@@@@@@@@@@@");
                   continue;
                }
                if (message.indexOf("ATZ") >= 0) {
                   System.out.println("\nSent OK");
                   data1[0] = XO;
                   data1[1] = XK;
                   data1[2] = CR;
                   data1[3] = LF;
                   o1.write(data1, 0, 4);
                   o1.flush();
                   System.out.println("@@@@@@@@@@@@@@@@@@@@@@@>");
                   System.out.write(data1, 0, 4);
                   System.out.println("<@@@@@@@@@@@@@@@@@@@@@@@");
                   continue;
                }
                if (message.indexOf("AT+CBST=7,0,1") >= 0) {
                   System.out.println("\nSent OK");
                   data1[0] = XO;
                   data1[1] = XK;
                   data1[2] = CR;
                   data1[3] = LF;
                   o1.write(data1, 0, 4);
                   o1.flush();
                   System.out.println("@@@@@@@@@@@@@@@@@@@@@@@>");
                   System.out.write(data1, 0, 4);
                   System.out.println("<@@@@@@@@@@@@@@@@@@@@@@@");
                   continue;
                }
                if (message.indexOf("AT+CPIN?") >= 0) {
                   System.out.println("\nsent +CPIN: READY");
                   data1[0] = PL;
                   data1[1] = XC;
                   data1[2] = XP;
                   data1[3] = XI;
                   data1[4] = XN;
                   data1[5] = CL;
                   data1[6] = BL;
                   data1[7] = XR;
                   data1[8] = XE;
                   data1[9] = XA;
                   data1[10] = XD;
                   data1[11] = XY;
                   data1[12] = CR;
                   data1[13] = LF;
                   o1.write(data1, 0, 14);
                   o1.flush();
                   System.out.println("@@@@@@@@@@@@@@@@@@@@@@@>");
                   System.out.write(data1, 0, 14);
                   System.out.println("<@@@@@@@@@@@@@@@@@@@@@@@");
                   continue;
                }
                 if (message.indexOf("AT+CREG?") >= 0) {
                   System.out.println("\nsent +CREG: 0,1");
                   data1[0] = PL;
                   data1[1] = XC;
                   data1[2] = XR;
                   data1[3] = XE;
                   data1[4] = XG;
                   data1[5] = CL;
                   data1[6] = BL;
                   data1[7] = X0;
                   data1[8] = CO;
                   data1[9] = X1;
                   data1[10] = CR;
                   data1[11] = LF;
                   o1.write(data1, 0, 12);
                   o1.flush();
                   System.out.println("@@@@@@@@@@@@@@@@@@@@@@@>");
                   System.out.write(data1, 0, 12);
                   System.out.println("<@@@@@@@@@@@@@@@@@@@@@@@");
                   continue;
                }
                if (message.indexOf("ATD") >= 0) {
                   System.out.println("\nsent CONNECT 9600/RLP");
                   data1[0] = XC;
                   data1[1] = XO;
                   data1[2] = XN;
                   data1[3] = XN;
                   data1[4] = XE;
                   data1[5] = XC;
                   data1[6] = XT;
                   data1[7] = BL;
                   data1[8] = X9;
                   data1[9] = X6;
                   data1[10] = X0;
                   data1[11] = X0;
                   data1[12] = SL;
                   data1[13] = XR;
                   data1[14] = XL;
                   data1[15] = XP;
                   data1[16] = CR;
                   data1[17] = LF;
                   o1.write(data1, 0, 18);
                   o1.flush();
                   System.out.println("@@@@@@@@@@@@@@@@@@@@@@@>");
                   System.out.write(data1, 0, 18);
                   System.out.println("<@@@@@@@@@@@@@@@@@@@@@@@");
                   System.out.println("Modem connect!!!!");
                   mc = true;
                   break;
                }
                if (message.indexOf("CONNECT") >= 0) {
                   System.out.println("Modem connect!!!!");
                   mc = true;
                   break;
                }
            }
         }
        catch (IOException e) {
            System.err.println("I/O error");
        }
        return mc;
    }

}



