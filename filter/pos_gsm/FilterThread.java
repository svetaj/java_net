import java.net.*;
import java.io.*;

public class FilterThread extends Thread {

    DataOutputStream out = null;
    DataInputStream in = null;
    String tn = null;

    public FilterThread(String str, DataOutputStream o, DataInputStream i) {
        super(str);
        out = o;
        in = i;
        tn = getName();
        System.out.println("Created new thread "+tn);
    }

    public void run() {
        try {
            byte[] data = new byte[512];
            int no_data;
            int count = 0;
            String message = null;
            System.out.println(tn+": data I/O start");
            while ((no_data = in.read(data)) != -1) {
                count++;
                System.out.print("\n"+tn+"["+count+"] START:");
                System.out.write(data, 0, no_data);
                System.out.print(":END "+tn+"["+count+"]");
                out.write(data, 0, no_data);
                out.flush();
                message = new String(data, 0, no_data, "US-ASCII");
                if (message.indexOf("+++") >= 0) {
                   System.out.println("$$$$$$$$$$$ +++ - Disconnect received !!!!");
                   break;
                }
            }
            System.out.println(getName()+": data I/O end");
        }
        catch (IOException e) {
            System.err.println(getName()+": Socket I/O error");
        }
    }
}
