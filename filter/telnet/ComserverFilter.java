import java.net.*;
import java.io.*;

public class ComserverFilter {
    public static void main(String[] args) throws IOException {

        DataOutputStream out1 = null;
        DataOutputStream out2 = null;
        DataInputStream in1 = null;
        DataInputStream in2 = null;


        Socket clientSocket2 = null;
        try {
            System.out.println("START:   connecting to socket 192.168.1.3, port 23");
            clientSocket2 = new Socket("192.168.1.3", 23);
            System.out.println("SUCCESS: connecting to socket 192.168.1.3, port 23");
            out2 = new DataOutputStream(clientSocket2.getOutputStream());
            in2 = new DataInputStream(clientSocket2.getInputStream());
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: 192.168.1.3.");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to: 192.168.1.3.");
            System.exit(1);
        }

        ServerSocket serverSocket1 = null;
        try {
            System.out.println("START:   creating server socket 192.168.1.1, port 4444");
            serverSocket1 = new ServerSocket(4444);
            System.out.println("SUCCESS: creating server socket 192.168.1.1, port 4444");
        } catch (IOException e) {
            System.err.println("Could not listen on port: 4444.");
            System.exit(1);
        }

        Socket clientSocket1 = null;
        try {
            System.out.println("START:   accepting connection to socket 192.168.1.1, port 4444");
            clientSocket1 = serverSocket1.accept();
            System.out.println("SUCCESS: accepting connection to socket 192.168.1.1, port 4444");
            out1 = new DataOutputStream(clientSocket1.getOutputStream());
            in1 = new DataInputStream(clientSocket1.getInputStream());
        } catch (IOException e) {
            System.err.println("Accept failed.");
            System.exit(1);
        }

        FilterThread ft1 = new FilterThread("socket1_to_socket2", out2, in1);
        FilterThread ft2 = new FilterThread("socket2_to_socket1", out1, in2);
        ft1.start();
        if (ft1 == null) System.out.println("FilterThread: socket1_to_socket2: null"); 
        ft2.start();
        if (ft2 == null) System.out.println("FilterThread: socket2_to_socket1: null"); 

        while (ft1.isAlive() && ft2.isAlive()) 
            try {            
                System.out.println("Filterthread: working ...");
                Thread.sleep(10000);
            } catch (InterruptedException e) {}

        out1.close();
        in1.close();
        out2.close();
        in2.close();
        clientSocket1.close();
        serverSocket1.close();
        clientSocket2.close();

    }
}

