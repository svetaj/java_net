import java.net.*;
import java.io.*;

public class FilterThread extends Thread {

    DataOutputStream out = null;
    DataInputStream in = null;
    boolean direction = false;
    // 123456FA3853CFDCAC5DCBE0C044DC1429CDCDD1
    static final char STX = '';
    static final char ETX = '';
    static final char H1  = 0;
    static final char H2  = 'Y';

    public FilterThread(String str, DataOutputStream o, DataInputStream i) {
        super(str);
        out = o;
        in = i; 
        System.out.println("Created new thread "+getName());
        if (getName().equals("socket1_to_socket2")) direction = true;
    }

    public void run() {
        try {
            byte[] data = new byte[512];
            int no_data;
            if (direction) { 
               while ((no_data = in.read(data)) != -1) {
                    System.out.println("\n"+getName()+": data I/O :");
                    System.out.write((STX+new String(data, 2, no_data-2)+ETX).getBytes(), 0, no_data);
                    out.write((STX+new String(data, 2, no_data-2)+ETX).getBytes(), 0, no_data);
                    out.flush();
               }
            }
            else {
               boolean header = true;
               while ((no_data = in.read(data,0,1)) != -1) {
                      if (header) {
                         System.out.write(H1);
                         out.write(H1);
                         System.out.write(H2);
                         out.write(H2);
                         header = false;
                      }
                      Byte xxx = new Byte(data[0]);
                      int yyy = xxx.intValue();
//                      System.out.println("yyy="+yyy+" STX="+STX+"=");
                      if (yyy == STX) continue;
                      if (yyy == ETX) continue;
                      System.out.write(data, 0, 1);
                      out.write(data, 0, 1);
                      out.flush();
               }
            }
            System.out.println(getName()+": data I/O end");
        } 
        catch (IOException e) {
            System.err.println(getName()+": Socket I/O error");
        }
    }
}
