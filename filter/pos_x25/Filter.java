import java.net.*;
import java.io.*;
import java.lang.*;

public class Filter {
    public static void main(String[] args) throws IOException {

        DataOutputStream out1 = null;
        DataOutputStream out2 = null;
        DataInputStream in1 = null;
        DataInputStream in2 = null;
        ServerSocket serverSocket1 = null;
        Socket clientSocket1 = null;
        Socket clientSocket2 = null;

        if (args.length == 0) {
            System.out.println("java Filter IPaddr1 port1 IPaddr2 port2");
            System.exit(1);
        }

        String ipaddr1 = args[0];
        int port1 = new Integer(args[1]).intValue();
        String ipaddr2 = args[2];
        int port2 = new Integer(args[3]).intValue();

        while (true) {

        try {
            System.out.println("START:   creating server socket "+ipaddr2+", port "+port2);
            serverSocket1 = new ServerSocket(port2);
            System.out.println("SUCCESS: creating server socket "+ipaddr2+", port "+port2);
        } catch (IOException e) {
            System.err.println("Could not listen on port: "+port2+".");
            System.exit(1);
        }

        try {
            System.out.println("START:   accepting connection to socket "+ipaddr2+", port "+port2);
            clientSocket1 = serverSocket1.accept();
            System.out.println("SUCCESS: accepting connection to socket "+ipaddr2+", port "+port2);
            out1 = new DataOutputStream(clientSocket1.getOutputStream());
            in1 = new DataInputStream(clientSocket1.getInputStream());
        } catch (IOException e) {
            System.err.println("Accept failed.");
            System.exit(1);
        }

        boolean mcc = modemConnect(in1);
        System.out.println("*************** modem connect = " + mcc);

        try {
            System.out.println("START:   connecting to socket "+ipaddr1+", port "+port1);
            clientSocket2 = new Socket(ipaddr1, port1);
            System.out.println("SUCCESS: connecting to socket "+ipaddr1+", port "+port1);
            out2 = new DataOutputStream(clientSocket2.getOutputStream());
            in2 = new DataInputStream(clientSocket2.getInputStream());
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: "+ipaddr1+".");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to: "+ipaddr1+".");
            System.exit(1);
        }

        FilterThread ft1 = new FilterThread(">>>>>>", out2, in1);
        FilterThread ft2 = new FilterThread("<<<<<<", out1, in2);
        ft1.start();
        if (ft1 == null) System.out.println("Filter: >>>>>>: null");
        ft2.start();
        if (ft2 == null) System.out.println("Filter: <<<<<<: null");

        while (ft1.isAlive() && ft2.isAlive())
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {}

        System.out.println("@@@@@@@@@ Closing I/O streams and sockets");
        out1.close();
        in1.close();
        out2.close();
        in2.close();
        clientSocket1.close();
        serverSocket1.close();
        clientSocket2.close();
	    }
    }

    static boolean modemConnect(DataInputStream i1) {
       boolean mc = false;
       try {
            byte[] data = new byte[512];
            int no_data;
            String message = null;

             while ((no_data = i1.read(data)) != -1) {
                message = new String(data, 0, no_data, "US-ASCII");
                System.out.write(data, 0, no_data);
                if (message.indexOf("CONNECT 1200") >= 0) {
                   System.out.println("Modem connect 1200 !!!!");
                   mc = true;
                   break;
                }
             }
         }
        catch (IOException e) {
            System.err.println("I/O error");
        }
        return mc;
    }

}



