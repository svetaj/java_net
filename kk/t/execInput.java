import	java.io.*;

class execInput {
    public static void main(String Argv[]) {
	try {
	    String ls_str;

	    Process ls_proc = Runtime.getRuntime().exec("set");

	    // get its output (your input) stream

	    DataOutputStream ls_in = new DataOutputStream(
                                          ls_proc.getOutputStream());

	    try {
		while ((ls_str = ls_in.readLine()) != null) {
		    System.out.println(ls_str);
		}
	    } catch (IOException e) {
		System.exit(0);
	    }
	} catch (IOException e1) {
	    System.err.println(e1);
	    System.exit(1);
	}

	System.exit(0);
    }
}
