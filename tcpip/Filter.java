import java.net.*;
import java.io.*;
import java.lang.*;

public class Filter {
    public static void main(String[] args) throws IOException {

        DataOutputStream out1 = null;
        DataOutputStream out2 = null;
        DataInputStream in1 = null;
        DataInputStream in2 = null;
        DataOutputStream ox1 = null;
        DataInputStream ix1 = null;


        if (args.length == 0) {
            System.out.println("java Filter destination IPaddr1 port1 source IPaddr2 port2");
            System.out.println("java Filter X25_PAD 192.168.1.3 23 POS_MOXA 192.168.1.1 4444");
            System.exit(1);
        }

        String src = args[0];
        String ipaddr1 = args[1];
        int port1 = new Integer(args[2]).intValue();
        String dest = args[3];
        String ipaddr2 = args[4];
        int port2 = new Integer(args[5]).intValue();

        FilterSocket fsClient1 = new FilterSocket(ipaddr1, port1, false);
        in1 = fsClient1.getInStream();
        out1 = fsClient1.getOutStream();

        FilterSocket fsClient2 = new FilterSocket(ipaddr2, port2, true);
        in2 = fsClient2.getInStream();
        out2 = fsClient2.getOutStream();

        FilterThread ft1 = new FilterThread(src, out2, in1, false);

        FilterThread ft2 = new FilterThread(dest, out1, in2, true);

        ft1.start();
        if (ft1 == null) System.out.println("Filter: "+src+": null");
        ft2.start();
        if (ft2 == null) System.out.println("Filter: "+dest+": null");

        while (ft1.isAlive() && ft2.isAlive())
            try {
                System.out.println("Filter: working ...");
                Thread.sleep(100000);
            } catch (InterruptedException e) {}


        fsClient1.endStream();
        fsClient2.endStream();

    }

}

