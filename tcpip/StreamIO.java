import java.net.*;
import java.io.*;
import java.util.Observable;

public class StreamIO extends Observable {

    DataOutputStream out = null;
    DataInputStream in = null;
    String sn = null;
    byte[] data = null;
    int no_data;
    int comStat = 0;


    public StreamIO(String s, DataOutputStream o, DataInputStream i) {
        sn = s;
        out = o;
        in = i;
        data = new byte[512];
        comStat = Protocol.STREAM_OPENED;
        setChanged();
        notifyObservers();
    }

    public void runFlow (boolean srv) {
        comStat = Protocol.DATA_IO_START;
        setChanged();
        notifyObservers();
        try {
            while ((no_data = in.read(data)) != -1) {
                comStat = Protocol.NEW_BLOCK_OF_DATA;
                setChanged();
                notifyObservers();
                if ((comStat = Protocol.analyze(data, no_data)) >= 0) {
                    setChanged();
                    notifyObservers();
			    }
                out.write(data, 0, no_data);
                out.flush();
            }
            comStat = Protocol.DATA_IO_END;
            setChanged();
            notifyObservers();
        }
        catch (IOException e) {
            comStat = Protocol.STREAM_IO_ERROR;
            setChanged();
            notifyObservers();
        }
    }

    public String getName() { return sn; }
    public byte[] getData() { return data; }
    public int getNoData() { return no_data; }
    public DataInputStream getInputStream() { return in; }
    public DataOutputStream getOutputStream() { return out; }
    public int getComStat() { return comStat; }

}
