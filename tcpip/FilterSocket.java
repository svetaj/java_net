import java.net.*;
import java.io.*;
import java.lang.*;

public class FilterSocket {
        DataOutputStream out = null;
        DataInputStream in = null;
        String ipaddr = null;
        int port;
        boolean server = false;
        boolean status = false;
        ServerSocket serverSocket = null;
        Socket clientSocket = null;

    public FilterSocket(String ip1, int p1, boolean srv) {

        ipaddr = new String(ip1);
        port = p1;
        server = srv;

        if (!server) {
            try {
                System.out.println("START:   connecting to socket "+ipaddr+", port "+port);
                clientSocket = new Socket(ipaddr, port);
                System.out.println("SUCCESS: connecting to socket "+ipaddr+", port "+port);
                out = new DataOutputStream(clientSocket.getOutputStream());
                in = new DataInputStream(clientSocket.getInputStream());
            } catch (UnknownHostException e) {
                System.err.println("Don't know about host: "+ipaddr+".");
            } catch (IOException e) {
                System.err.println("Couldn't get I/O for the connection to: "+ipaddr+".");
            }
            status = true;
        }
        else {
 
           try {
               System.out.println("START:   creating server socket "+ipaddr+", port "+port);
               serverSocket = new ServerSocket(port);
               System.out.println("SUCCESS: creating server socket "+ipaddr+", port "+port);
           } catch (IOException e) {
               System.err.println("Could not listen on port: "+port+".");
           }

           try {
               System.out.println("START:   accepting connection to socket "+ipaddr+", port "+port);
               clientSocket = serverSocket.accept();
               System.out.println("SUCCESS: accepting connection to socket "+ipaddr+", port "+port);
               out = new DataOutputStream(clientSocket.getOutputStream());
               in = new DataInputStream(clientSocket.getInputStream());
           } catch (IOException e) {
               System.err.println("Accept failed.");
           }
           status = true;
        }

    }

    public void endStream() {
        try {
            out.close();
            in.close();
            clientSocket.close();
            if (server) serverSocket.close();
        } catch (IOException e) {
            System.err.println("Close failed.");
        }
    }

    public DataOutputStream getOutStream() { return out;}

    public DataInputStream getInStream() { return in;}

    public boolean getStatus() { return status;}
}

