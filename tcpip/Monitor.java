import java.util.Observer;
import java.util.Observable;

public class Monitor implements Observer {

    String tn = null;
    byte[] data = null;
    int no_data;
    int comStat = 0;
    int count = 0;
    private StreamIO sio = null;

    public Monitor(StreamIO sio) {
        this.sio = sio;
        data = new byte[512];
    }

    public void update( Observable obs, Object obj )
    {
        if (obs == sio)
           Protocol.display( (StreamIO) sio );
    }


    public int getCommStat() { return comStat; }
}






