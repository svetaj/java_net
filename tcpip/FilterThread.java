import java.net.*;
import java.io.*;
import java.util.Observable;

public class FilterThread extends Thread {

    DataOutputStream out = null;
    DataInputStream in = null;
    String tn = null;
    StreamIO sio = null;
    Monitor mon = null;
    boolean server = false;

    public FilterThread(String str, DataOutputStream o, DataInputStream i, boolean srv) {
        tn = str;
        server = srv;
        sio = new StreamIO (tn, o, i);
        mon = new Monitor(sio);
        sio.addObserver(mon);
    }

    public void run() {
        sio.runFlow(server);
    }
}
