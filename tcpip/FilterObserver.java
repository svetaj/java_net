import java.net.*;
import java.io.*;
import java.util.Observable;

public class Protocol implements Observer, Runnable {

    DataOutputStream out = null;
    DataInputStream in = null;
    String tn = null;
    byte[] data = null;
    int no_data;
    final static int THREAD_CREATED = 0;
    final static int SOCKET_IO_ERROR = 1;
    final static int NEW_BLOCK_OF_DATA = 2;
    final static int DATA_IO_START = 3;
    final static int DATA_IO_END = 4;
    int comStat = 0;

    public ProtocolObserver() {
        data = new byte[512];
        System.out.println("Created new thread "+tn);
    }


    public void run() {
        try {
            System.out.println(tn+": data I/O start");
            while ((no_data = in.read(data)) != -1) {
                setChanged();
                notifyObservers();
                System.out.print("\n"+tn+"["+count+"] START:");
                System.out.write(data, 0, no_data);
                System.out.print(":END "+tn+"["+count+"]");
                out.write(data, 0, no_data);
                out.flush();
            }
            System.out.println(getName()+": data I/O end");
        } 
        catch (IOException e) {
            System.err.println(getName()+": Socket I/O error");
        }
    }

    public void update( Observable obj, Object arg )
    {
        protocol( arg );
    }

    void protocol( Object arg )
    {
        tn = arg.getName();
        data = arg.getData();
        no_data = arg.getNoData();
        in = arg.getInputStream();
        out = arg.getOutputStream();

    }
}






