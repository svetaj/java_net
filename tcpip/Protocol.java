import java.net.*;
import java.io.*;

public class Protocol {

    static int stateServer = -1;
    static int stateClient = -1;
    static boolean actionServerReq = false;
    static boolean actionClientReq = false;
    static int actionServer = -1;
    static int actionClient = -1;

    final static int STREAM_OPENED = 0;
    final static int STREAM_IO_ERROR = 1;
    final static int NEW_BLOCK_OF_DATA = 2;
    final static int DATA_IO_START = 3;
    final static int DATA_IO_END = 4;
    final static int TWO_BYTE_ENQ = 5;
    final static int MODEM_CONNECT = 6;
    final static int DLE_EOT = 7;
    final static int ACK_REPLY = 8;
    final static int DISCONNECT = 9;
    final static int HANGUP = 10;
    final static int MODEM_OK = 11;
    final static String CON = "CONNECT 1200";
    final static String DIS = "+++";
    final static String HNG = "ATH";
    final static String MOK = "OK";
    final static String COD = "US-ASCII";
    final static byte ENQ = 0x05;  // Enquery
    final static byte CR  = 0x0D;  // Carriage return
    final static byte LF  = 0x0A;  // Line feed
    final static byte DLE = 0x10;  // Data Link Escape
    final static byte EOT = 0x04;  // End of tape
    final static byte ACK = 0x06;  // Acknoledgment
    final static byte STX = 0x02;  // Start of text
    final static byte ETX = 0x03;  // End of text

    public static int protAction ( boolean server, boolean newState )
    {
        int s = -1;
        System.out.println("server=" + server + " newState=" + newState);
        return s;
    }

    public static int analyze (byte[] b, int bl) {
	   boolean state_changed = false;
       int comStat = -1;
       try {
          String message = new String(b, 0, bl, COD);
          if (message.indexOf(CON) >= 0) {
		      comStat = MODEM_CONNECT;
		      state_changed = true;
   		  }
		  if (message.indexOf(LF) >=0 &&  message.indexOf(ENQ) >=0) {
		      comStat = TWO_BYTE_ENQ;
		      state_changed = true;
		  }
		  if (message.indexOf(DLE) >=0 &&  message.indexOf(EOT) >=0) {
		      comStat = DLE_EOT;
		      state_changed = true;
		  }
		  if (message.indexOf(ACK) >=0 &&  message.indexOf(STX) >=0) {
			  comStat = ACK_REPLY;
		      state_changed = true;
		  }
		  if (message.indexOf(DIS) >=0) {
			  comStat = DISCONNECT;
		      state_changed = true;
		  }
		  if (message.indexOf(HNG) >=0) {
			  comStat = HANGUP;
		      state_changed = true;
		  }
		  if (message.indexOf(MOK) >=0) {
			  comStat = MODEM_OK;
		      state_changed = true;
		  }
	   }
       catch (UnsupportedEncodingException e) {}
       return comStat;
    }


    public static void display( StreamIO arg )
    {
        String tn = arg.getName();
        byte[] data = arg.getData();
        int no_data = arg.getNoData();
        int comStat = arg.getComStat();

        switch (comStat) {
         case STREAM_OPENED:
            System.out.println("Stream "+tn+": opened");
            break;
         case STREAM_IO_ERROR:
            System.out.println(tn+": Stream I/O error");
            break;
         case NEW_BLOCK_OF_DATA:
            System.out.print("\n"+tn+" START:");
            System.out.write(data, 0, no_data);
            System.out.print(":END "+tn);
            break;
         case DATA_IO_START:
            System.out.println(tn+": data I/O start");
            break;
         case DATA_IO_END:
            System.out.println(tn+": data I/O end");
            break;
         case TWO_BYTE_ENQ:
            System.out.println(tn+": *** TWO_BYTE_ENQ ***");
            break;
         case MODEM_CONNECT:
            System.out.println(tn+": *** MODEM_CONNECT ***");
            break;
         case DLE_EOT:
            System.out.println(tn+": *** DLE_EOT ***");
            break;
         case ACK_REPLY:
            System.out.println(tn+": *** ACK_REPLY ***");
            break;
         case DISCONNECT:
            System.out.println(tn+": *** DISCONNECT ***");
            break;
         case HANGUP:
            System.out.println(tn+": *** HANGUP ***");
            break;
         case MODEM_OK:
            System.out.println(tn+": *** MODEM_OK ***");
            break;
        }
    }


}

